---
layout: markdown_page
title: Product Direction - Growth
description:
---

## Growth Section Overview



## Growth OKRs & KPIs

The growth team at GitLab is new and we are iterating along the way. As of August 2019 we have just formed the expansion group and are planning to become a more mature, organized and well oiled machine by January 2020. 

## Problems we solve

### 1. Key User workflows: 

### 2. Key User Focus:

#### External
The external user personas on GitLab.com and our Self-Managed instances are very different and should be treated as such. We will be focusing on the 2 described below. 

In addition to the personas, it's also important to understand the permissions available for users (limits and abilities) at each level. 
* [Handbook page on permissions in GitLab](https://about.gitlab.com/handbook/product/#permissions-in-gitlab)
* [Docs on permission on docs.gitlab.com](https://docs.gitlab.com/ee/user/permissions.html)
* *Note: GitLab.com and Self-Managed permission do differ.*  

#### GitLab Team Members
*  Sales Representatives
*  Customer Success Representatives
*  Support Representatives

### 3. Apps & Services we focus on:
*   [GitLab Enterprise Edition (Self-Managed)](https://about.gitlab.com/handbook/engineering/projects/#gitlab)
*   [GitLab-com (SaaS)](https://gitlab.com/gitlab-com/www-gitlab-com#www-gitlab-com)
*   [Version.GitLab](https://about.gitlab.com/handbook/engineering/projects/#version-gitlab-com)
*   [Customers.GitLab](https://about.gitlab.com/handbook/engineering/projects/#customers-app)
*   [License.GitLab](https://about.gitlab.com/handbook/engineering/projects/#license-app)

## Growth Initiatives Board

* Acquisition board
* Conversion board
* Retention Board
* Fulfillment Board 
* Telemetry Board 
* Expansion Board 

### What's Next
12.10


13.0



### Helpful Links
*   [Growth Section](https://about.gitlab.com/handbook/engineering/development/growth/)
*   [Growth Product Handbook](https://about.gitlab.com/handbook/product/growth/)
*   [UX Scorecards for Growth](https://gitlab.com/groups/gitlab-org/-/epics/2015)
*   [GitLab Growth project](https://gitlab.com/gitlab-org/growth)
*   [KPIs & Performance Indicators](https://about.gitlab.com/handbook/product/metrics/)
