---
layout: handbook-page-toc
title: "Field Certification Program"
---

# Field Certification Program 
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}




# Field Certification Program Overview 
To support and scale GitLab’s continued growth and success, the Field Enablement Team is developing a role based certification program that includes functional, soft skills, and technical training for field associates.  

The certification badges will bw aligned to the customer journey and the critical “Moments That Matter” (MTMs) as well as role-based competencies that address the critical knowledge, skills, behaviors, processes, content, and tools to successfully execute each MTM.

### What is certification? 
The act of validating competency of an individual to prove that:
* Knowledge has been transferred (Do you know it?)
* The learner can apply that knowledge in simulated & live scenarios (Can you do it?)
* The learner has taken ownership of effective skill execution and can demonstrate proficiency over time (Can you do it independently and continue to execute over time?)

### Why Certification? 
Certification programs allow us to validate GitLab skills and knowledge, recognize indivudal mastery, and motivarte continuous learning and skills growth all aligned to expectation setting and performance discussions. 

## Audience 
The Field Certification Program will include:
* Functional and soft skills training for Strategic Account Leaders and Account Executives  
* Functional and soft skills training for Solutions Architect and Techncial Account Managers

## Certification Levels 

| Level Name        | Skills Level    | Course/ Exam Numbering Scheme | Color Highlight | Validation Description                                                      |
|-------------------|-----------------|-------------------------------|-----------------|-----------------------------------------------------------------------------|
| Level 1: Learning | Knowledgable    | 101                           | Yellow          | Has essential skills                                                        |
| Level 2: Growing  | Intermediate    | 201                           | Orange          | Has deep skills in specialized area                                         |
| Level 3: Thriving | Advanced        | 301                           | Purple          | Has broad skills across multiple areas                                      |
| Level 4: Expert   | Highly-Mastered | 401                           | Red             | Keeps up to daye each month + possesses "professional" level certification  |

## Certification Assessments
To achieve GitLab “certified” status, candidates must complete both the online written exam (knowledge) and practical assessment (skill) with a passing score on each exam.
* Anyone can sign up and take the written exam but only candidates who successfully pass the online written exam will be granted access to the practical assessment
* The practical assessment will consist of demonstrated ability to perform Moments that Matter to GitLab’s standards 

## Moments That Matter (MTM)
Moments That Matters are episodes where a sales representative creates engagements with a customer that make a lasting emotional impression on how they feel about them.
They are based on the concept explained in "Blueprints for a SAAS Sales Organization: How to ?Design, Build and Scale a Customer Centric Sales Orgnization" by Jacob Van Der Kooj and Fernando Pizarro. 

## GitLab Moments That Matter 
The GitLab Moments That Matter are still be ironed out. To see that latest check out the [slide](https://docs.google.com/presentation/d/1mhzoJMJSyx4wz47g-0P2dUQJc2hTHflZUhE57mcN83g/edit#slide=id.g711c708e57_1_7)

## Roadmap 
*Coming soon* 






