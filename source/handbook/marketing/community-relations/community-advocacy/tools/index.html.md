---
layout: handbook-page-toc
title: "Community advocacy tools"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

- [Disqus](/handbook/marketing/community-relations/community-advocacy/tools/disqus)
- [Zendesk](/handbook/marketing/community-relations/community-advocacy/tools/zendesk)
